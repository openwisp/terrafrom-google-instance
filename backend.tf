terraform {
  backend "gcs" {
    bucket = "openwisp-215513-tfstate"
    credentials = "./creds/serviceaccount.json"
  }
}
