image:
  name: registry.gitlab.com/hispanico/docker-terraform-ansible:latest
  entrypoint:
    - '/usr/bin/env'
    - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'

variables:
  ANSIBLE_HOST_KEY_CHECKING: "False"
  ANSIBLE_FORCE_COLOR: "1"

before_script:
  - rm -rf .terraform
  - terraform --version
  - mkdir -p ./creds
  - mkdir -p ./keys
  - echo $SERVICEACCOUNT | base64 -d > ./creds/serviceaccount.json
  - echo $ID_RSA_PUB | base64 -d > ./keys/id_rsa.pub
  - echo $ID_RSA | base64 -d > ./keys/id_rsa
  - chmod 600 ./keys/id_rsa
  - terraform init

stages:
  - validate
  - plan
  - apply
  - provision
  - idempotence
  - destroy

terraform-validate:
  stage: validate
  script:
    - terraform validate

ansible-syntax:
  stage: validate
  script:
    - ansible-galaxy install -r ansible/requirements.yml
    - ansible-playbook ./ansible/test.yml --syntax-check

ansible-lint:
  stage: validate
  script:
    - ansible-galaxy install -r ansible/requirements.yml
    - ansible-lint ./ansible/test.yml
  allow_failure: true

terraform-plan:
  stage: plan
  script:
    - terraform plan -out "planfile"
  dependencies:
    - terraform-validate
  artifacts:
    paths:
      - planfile

terraform-apply:
  stage: apply
  script:
    - terraform apply -input=false "planfile"
  dependencies:
    - terraform-plan
  artifacts:
    paths:
      - ansible_inventory

ansible-provision:
  stage: provision
  script:
    - ansible-galaxy install -r ansible/requirements.yml
    - ansible-playbook -u ansible -i "ansible_inventory" --private-key ./keys/id_rsa ./ansible/test.yml
  dependencies:
    - terraform-apply

ansible-idempotence:
  stage: idempotence
  script:
    - ansible-galaxy install -r ansible/requirements.yml
    - ansible-playbook -u ansible -i "ansible_inventory" --private-key ./keys/id_rsa ./ansible/test.yml --diff | tee -a idempotence.log
    - >
      tail idempotence.log
      | grep -q 'changed=0.*failed=0'
      && (echo 'Idempotence test: pass' && exit 0)
      || (echo 'Idempotence test: fail' && exit 1)
  dependencies:
    - terraform-apply

terraform-destroy_on_failure:
  stage: destroy
  script:
    - terraform destroy -auto-approve
  when: on_failure

terraform-destroy:
  stage: destroy
  script:
    - terraform destroy -auto-approve
