provider "google" {
  credentials = "${file("./creds/serviceaccount.json")}"
  project     = "openwisp-215513"
  region      = "europe-west4"
}
