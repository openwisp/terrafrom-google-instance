resource "google_compute_instance" "tf-openwisp2" {
  count = 4
  name = "tf-compute-${element(var.distros, count.index)}"
  machine_type = "f1-micro"
  zone = "europe-west4-a"

  tags = ["${element(var.distros, count.index)}", "openwisp", "http-server", "https-server"]

  boot_disk {
    initialize_params {
      image = "${element(var.images, count.index)}"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }

  metadata {
    sshKeys = "ansible:${file(var.ssh_public_key)}"
  }

  provisioner "remote-exec" {
    inline = [
      "if [ -e /usr/bin/yum ]; then sudo yum install -y python; fi",
      "if [ -e /usr/bin/apt ]; then sudo apt install -y python; fi",
    ]
    connection {
      type        = "ssh"
      user        = "ansible"
      private_key = "${file(var.ssh_key_private)}"
    }
  }

  provisioner "local-exec" {
    command = <<EOD
cat <<EOF >> ansible_inventory
${element(var.distros, count.index)}.${self.network_interface.0.access_config.0.assigned_nat_ip}.nip.io
EOF
EOD
  }
}
