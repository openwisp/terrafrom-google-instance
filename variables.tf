variable "images" {
  description = "OS Images"
  type = "list"
  default = ["ubuntu-os-cloud/ubuntu-1804-lts","ubuntu-os-cloud/ubuntu-1604-lts", "centos-cloud/centos-7","debian-cloud/debian-9"]
}
variable "distros" {
  description = "OS distro"
  type = "list"
  default = ["ubuntu1804", "ubuntu1604", "centos7", "debian9"]
}
variable "ssh_public_key" {
  description = "ssh public key"
  type        = "string"

  default = "./keys/id_rsa.pub"
}

variable "ssh_key_private" {
  description = "ssh private key"
  type        = "string"

  default = "./keys/id_rsa"
}
